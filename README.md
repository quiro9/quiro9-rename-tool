WARNING
============================
Important: this code is very old I will never work on it, if you download it you have to adapt it to the new wordpress version, without mentioning that you need filters for the vars in the functions "fwrite();"


Quiro9 Rename Tool
============================

### for wordpress 4.0!
There is still much room for improvement , you can report bugs on this site and feel free to modify the code to improve it ...

Description
--------------------
This plugin renames the "wp-admin" folder "wp-conten", "wp-includes" and "wp-login.php " and "wp-configure.php" files, modifying everything to work well. For now it is recommended only for advanced users and programmers...

* Please use carefully ... Set up properly (do not apply changes but sure to set it).
* It is recommended to first configure all your wordpress (pluggins and themes) and only after applying this plugin (so will have less problems)


Changelog
------------------
#### v 0.80 (this master)
* Reorganization of various functions
* Changed some lines of code

#### v 0.71
* Bugs fixed
* All text in English
* Optimize the code
* Add fuctions and lines Comments

#### v 0.65
* Scan to rename folders and files...
* minimun security of the name rules and errors
* search content for all .php .html .js .css (text files) and rename all in the content to the correctly funcionality...
* change content of new files (manually), rename content in new updates (pluggins and themes)
* Option to configure the name of folders
* skip the pluggin folder, .pot .po .mo (languajes files) and .gif .png .jpg .jpeg .bmp .svg (images files)

#### Work, In the next versions:
* Correction important bug: sometimes not to rename some files.
* auto rename script in new updates (for plugins and themes) 
* auto backups (for all wordpress folders...) 
* individual auto backups for plugins installer (before activate plugin)...
* auto restore (feedback for all content)...

#### Before
* internal release...
* Optimize the code...


Installation
--------------------
* Dowload the install.zip

	* Or download [the master .zip master](https://github.com/quiro9/quiro9-rename-tool/archive/master.zip), unzip this; into a "quiro9-rename-tool-master" folder, create a new .zip of the "quiro9-rename-tool" (excluye the .md file. Tt's new .zip is for install)
	
* just upload and activate the pluggin ...

* Then read carefully ( in Spanish ) and configure all...

* Apply the changes (recommended after configuring your theme and pluggins in wordpress...)

__________________________
Please, if you modify the code you need try to comment in English.
Here on github your can use English or Spanish in Issues or Requests...
