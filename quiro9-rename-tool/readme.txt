﻿warning: this code is very old I will never work on it, if you download it you have to adapt it to the new wordpress version, without mentioning that you need filters for the vars in the functions "fwrite();"

=== Quiro9 Rename Tool ===
Contributors: quiro9
Tags: admin tool, rename, quiro9
Requires at least: 4.0
Tested up to: 4.0
Stable tag: 0.80
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

This plugin renames the "wp-admin" folder "wp-conten", "wp-includes" and "wp-login.php " and "wp-configure.php" files, modifying everything to work well. For now it is recommended only for advanced users and programmers...

== Description ==

This plugin renames the "wp-admin" folder "wp-conten", "wp-includes" and "wp-login.php " and "wp-configure.php" files, modifying everything to work well. For now it is recommended only for advanced users and programmers...

### you can create your tunk or contribute here:
[GitHub Developer](https://github.com/quiro9/wordpress_quiro9_rename_tool)

== Installation ==
* Dowload the install.zip

* Or download [the master .zip master](https://github.com/quiro9/quiro9-rename-tool/archive/master.zip), unzip this; into a "quiro9-rename-tool-master" folder, create a new .zip of the "quiro9-rename-tool" (excluye the .md file. Tt's new .zip is for install)

* just upload and activate the pluggin ...


* Then read carefully ( in Spanish ) and configure all...


* Apply the changes (recommended after configuring your theme and pluggins in wordpress...)

== Changelog ==

== v 0.80 (this master) ==
* Bugs fixed
* All text in English

== 0.65 ==
* Scan to rename folders and files...
* minimun security of the name rules and errors
* search content for all .php .html .js .css (text files) and rename all in the content to the correctly funcionality...
* change content of new files (manually), rename content in new updates (pluggins and themes)
* Option to configure the name of folders
* skip the pluggin folder, .pot .po .ln (languajes files) and .gif .png .jpg .jpeg .bmp .svg (images files)

== Before ==
* internal release...
* bugs correction...
* Optimize code...


== Upgrade Notice ==
Noting
