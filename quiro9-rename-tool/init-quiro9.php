<?php
/*
Plugin Name: Quiro9 Rename Tool
Plugin URI: https://github.com/quiro9/quiro9-rename-tool
Description: This Plugin is licensed under (GNU/GPL3)...<br/> The tool allows you to rename folders wp-admin, wp-content, wp-includes and the PHP files wp-login y wp-config.<br/><b>Activate this plugin </b> and go to <a href='./options-general.php?page=quiro9_rename_tool'> Settings > "Quiro9 Admin Tools" </a> for configuring and executing it ...
Author: Juan Quiroga
Author: quiro9
Author URI: https://github.com/quiro9
Requires at least: 4.0
Version: 0.80
*/
/*  Derechos de Autor 2014 Juan Quiroga (correo electrónico: juanquiroga9@gmail.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY and FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */
    /*
     * @package Quiro9 Rename tool
     * @subpackage WordPress Plugin
     * @description Rename folders wp- for security and personalitation...
     * @install The configuration is for advance user or programers
     * @branche 2014
     * @since   4.0+
     * @tested  4.0+
     * @version 0.71
     * @author quiro9
     * @license GPLv3 or later
     */

//-----------------------------------------------------------------

global $wp_version;

// This is a menu options and auto init for upload for this plugin...
function quiro9_menu()
{
     add_options_page ('q9 rename tool', 'Quiro9 Rename Tool', 'manage_options', 'quiro9_rename_tool', 'quiro9_menu_options');
}

// it is when you press the menu
function quiro9_menu_options()
{
	try 
	{
		$nonceq9 = wp_create_nonce ('q9_apply');
		$q9_action = './options-general.php?page=quiro9_rename_tool&status=q9_ok';
		$q9_plugin_dir = dirname( __FILE__ );
		require_once( $q9_plugin_dir.'/php/lib-essentials.php');
		require_once( $q9_plugin_dir.'/php/lib-rename.php');
		wp_enqueue_style( 'quiro9-rename-tool', plugins_url('css/style.css', __FILE__));
	} 
	catch (Exception $e) 
	{
		wp_die (__ ('<br/>Error: no estan las libreriras necesarias<br/>'));
	}
	// "draw" interface and call funtions
	echo "
	<div class = 'wrap'>
		<div class='logo'></div>
			<div class='intro'>
				<center><img src='".content_url()."/plugins/quiro9-rename-tool/img/q9-logo.png'>
				<h1> Welcome to Quiro9 Rename Tool</h1><br/>
				<p><b>The plugin was intended to wordpress 4.x (License GNU/GPL3).</b><br/>By <a href='mailto:juanquiroga9@gmail.com'>Juan Quiroga</a>...</p>
            </div>";
            if($_POST)
            {
                if($_POST['confirm'])
                {
					global $replace_admin, $replace_includes, $replace_content, $replace_login, $replace_config;
					q9_global_orignal_vars();
					// here is to aply the changes
					if($_POST['confirm']=='apply' and $_GET['status']=='q9_ok' and wp_verify_nonce($nonceq9, 'q9_apply'))
					{
							// get for post method all names (and convert to lower) if is blank rename to orignal name...
							if($_POST['quiro9_wp_name_admin']!=""){$replace_admin = strtolower ($_POST['quiro9_wp_name_admin']);}else{$replace_admin = $search_original_admin;}
							if($_POST['quiro9_wp_name_content']!=""){$replace_content = strtolower ($_POST['quiro9_wp_name_content']);}else{$replace_content = $search_original_content;}
							if($_POST['quiro9_wp_name_includes']!=""){$replace_includes = strtolower ($_POST['quiro9_wp_name_includes']);}else{$replace_includes = $search_original_includes;}
							if($_POST['quiro9_wp_name_login']!=""){$replace_login = strtolower ($_POST['quiro9_wp_name_login']);}else{$replace_login = $search_original_login;}
							if($_POST['quiro9_wp_name_config']!=""){$replace_config = strtolower ($_POST['quiro9_wp_name_config']);}else{$replace_config = $search_original_config;}
							// minimun comprobatesand validates (diferent names) and init for 0-9 or a-z
							if((((($replace_admin != $replace_content) != $replace_admin) != $replace_includes) != $replace_login) != $replace_config)
							{
								if(q9_validate($replace_admin) and q9_validate($replace_content) and q9_validate($replace_includes) and q9_validate($replace_login) and q9_validate($replace_config))
								{
									$replace_login .= ".php";$replace_config .= ".php";
									q9_rename_all();
								}
								else
								{
									wp_die (__ ('<br/>Error, A utilizado caracteres invalidos en los nombres?...<br/>'));
								}
							}
							else
							{
								wp_die (__ ('<br/>Error, A escrito dos nombres iguales...<br/>'));
							}
					}
					// here is to revert (earse all to the data base)
					else if($_POST['confirm']=='revert' and $_GET['status']=='q9_ok' and wp_verify_nonce($nonceq9, 'q9_apply'))
					{
						q9_data_earse();
					}
					else
					{
						wp_die (__ ('<br/>Error, No ha confirmado el cambio o no tiene los permisos suficientes...<br/>'));
					}
                }
            }
            else
			{   // this is when the change are apply
                    if(get_option('quiro9_wp_name_admin')){

						echo "<center><h2>If you don't reverse the changes, don't disable this plugin</h2><b>You may need to reconcile new themes or plugins</b>";

						$change_content = get_option('quiro9_wp_name_content');
						echo "<p>You renamed <u>\"wp-admin\"</u> for: <b>".get_option('quiro9_wp_name_admin')."</b><br/>";
                        echo "You renamed <u>\"wp-content\"</u> for: <b>".$change_content."</b><br/>";
                        echo "You renamed <u>\"wp-includes\"</u> for: <b>".get_option('quiro9_wp_name_includes')."</b><br/>";
                        echo "You renamed <u>\"wp-login.php\"</u> for: <b>".get_option('quiro9_wp_name_login')."</b><br/>";
                        echo "You renamed <u>\"wp-config.php\"</u> for: <b>".get_option('quiro9_wp_name_config')."</b></p><br/>";

                        echo "<p><form action='".$q9_action."' method='POST' >
                        <u>When applying changes take a minute (Wait and do not close this page until you finalize)...</u><br/>
                        *** <b>Enter the URL (step back from this URL)</b> ***<br/>
                        <input type='text' name='q9_manual_update' value='../".$change_content."/' placeholder='../".$change_content."/' >
                        <button type='submit'>Update Plugin or Theme (manually)</button></form>";
                        if($_POST)
                        { //if submit q9_manual_update (this form)
							if($q9_up_dir = $_POST['q9_manual_update'])
							{
								echo "<div style='color:#f35;'>";
								if(is_dir($q9_up_dir) and !strpos($q9_up_dir,"../..") and !strpos($q9_up_dir,"..\\..") and !strpos($q9_up_dir,"...") )
								{
									q9_new_update($q9_up_dir);
								}
								else
								{
									echo "Invalid directory, make sure recorer completely (go back only once if necessary whit '../' ) and retry ...";
								}
								echo "</div><br/>";
							}
						}
						echo "</p><br/>
						<p><h3><b>If you want to roll back the changes restore your previous backup (replacing all contents)...</b></h3>
						<b>If ralizo backup (or lost) is required to restore your entire wordpress (including plugins and themes) to remove everything changed ...
Then press the 'delete database' button if you want to apply new settings ...</b><br/>";

						echo "<form name='theform' action='".$q9_action."' method='POST' >
						<p><b><u>This only deletes the data from the database (do not apply if not reversed all previously wordpress content):</u></b></p>";
						echo "<br/><input type='checkbox' id='confirm' name='confirm' value='revert' onclick='document.theform.apply.disabled=!document.theform.apply.disabled'; >
						I understand and I wish to delete all data stored on this program ...<br/>
						<button type='submit' name='apply' disabled>Clear all Data</button></form></p></center>";
				}
				else
				{ // this is the configuration
                            echo "<center><u>When you apply the changes expected to complete (It will take 1 or 2 minutes). When complete press the link that will be given at the end of this page...</u><br/>
                            <h2>This can't reverse 'Automatically' the changes<br/>
                            <b>Create a backup and store this off the wordpress or in the plugin folder</b></h2>";

                            echo "<p><b>It is recommended use this plugin when you already have everything set up (plugins and themes) in wodpress!</b><br/>
                            This plugin tries to reconcile everything (whit plugin and themes), but there may be some drawbacks...
                            <b>if not a developer or advanced user, we recommend NOT USE IT</b>...<br/>

                            If you want to add plugins or themes after applying updates to this plugin, you must (before turning) 'apply changes' by the option given here...</p>

                            <p><h3>Tips and information to not cause problems to your Wordpress:</h3>
                            * <b>Start the name with just letters or numbers ...</b><br/>
                            * <b>Avoid symbols, the only symbol allowed is the middle region </b> (-)<br/>
                            * <b>Do not use space in blank or tab key...</b><br/>
                            * <b>Avoid using a similar names...</b><br/>
                            * <b>The names should be the same as other or contain: \"wp-admin\", \"wp-content\", \"wp-includes\", \"wp-login\" or \"wp-config\"</b><br/><u>(in case of not wanting to change a file or folder, leave blank)</u></p>";

                            $randname = substr(md5(md5(rand(999,9999999999),rand(6,12))),rand(0,16),rand(3,6));
                            $randval1 = rand(0,3);$randval2 = rand(0,3);$randval3 = rand(0,3);
                            $randadmin = array('users','admin','adm','administrator');
                            $randlog = array('login','init','log','start');
                            $randinc = array('libs','includes','inc','func');
                            $randconf = array('saves','config','cnf','data');
                            $randcont = array ('wall','content','cnt','add');

                            // this is the form to configure the names of folder and files
                            echo "<form name='theform' action='".$q9_action."' method='POST' >
                            <p>Here enter the names for Renaming:</p>
                            wp-admin:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='quiro9_wp_name_admin' maxlength='20' id='quiro9_wp_name_admin' value='".$randadmin[$randval1]."' placeholder='".$randadmin[$randval1]."'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
                            wp-includes&nbsp;&nbsp;&nbsp;
                            <input type='text' name='quiro9_wp_name_includes' id='quiro9_wp_name_includes' maxlength='20' value='".$randinc[$randval3]."-".$randname."' placeholder='".$randinc[$randval3]."-".$randname."'  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
                            wp-content:&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='quiro9_wp_name_content' maxlength='20' id='quiro9_wp_name_content' value='".$randcont[$randval3]."-".$randname."' placeholder='".$randcont[$randval3]."-".$randname."'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
                            wp-login:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type='text' name='quiro9_wp_name_login' id='quiro9_wp_name_login' maxlength='20' value='".$randlog[$randval2]."' placeholder='".$randlog[$randval2]."'/>.php<br/>
                            wp-config:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type='text' name='quiro9_wp_name_config' id='quiro9_wp_name_config' maxlength='20' value='".$randconf[$randval3]."-".$randname."' placeholder='".$randconf[$randval3]."-".$randname."'/>.php<br/>";

                            echo "<br/><input type='checkbox' id='confirm' name='confirm' value='apply' onclick='document.theform.apply.disabled=!document.theform.apply.disabled'; >
                            I have successfully completed the configuration and I understand the risks...<br/>";
							echo "<button type='submit' name='apply' disabled >Apply Changes</button></form></p></center>";
		}
	}
	echo "</div>";
}

// HERE INIT THE PLUGIN!
if ($wp_version >= 4.0 and defined( 'ABSPATH' ))
{
        if (is_admin()) 
        {
			add_action ('admin_menu', 'quiro9_menu');
		}
        // here should call the changes when new themes or plugins eg:
        // add_action('upload','q9_update_prosess')
}
else
{
	wp_die( __( 'This Plugin Requires WordPress 4.0 or Greater: Activation Stopped!' ) );
}

?>
