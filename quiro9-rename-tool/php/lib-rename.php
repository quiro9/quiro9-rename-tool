<?php
/*  Derechos de Autor 2014 Juan Quiroga (correo electrónico: juanquiroga9@gmail.com)
   You should have received a copy of the GNU General Public License <https://www.gnu.org/copyleft/gpl.html>. */

//************************************************//
// DONT TOUCH IT, rename the dirs take in array() //
//************************************************//
function q9_rename_dirs($rename_dirs = array(),$search_dirs,$replace_dirs)
{
    echo "<br/><br/><b> *** Folders and files will be replaced ".$search_dirs." for ".$replace_dirs." ***</b><br/>";
    $errors = 0;
    foreach($rename_dirs as $rnm_dir)
    {
        if(file_exists($rnm_dir))
        {
            $new_name = str_replace($search_dirs,$replace_dirs,$rnm_dir);
            if(!rename($rnm_dir,$new_name))
            {
				echo "No pudo renombrarse: ".$rnm_dir."<br/>";$errors++;
			}
         }
        else
        {
			echo $rnm_dir." It was renamed?...<br/>";$errors++;
		}
    }
    echo "<b> *** Finish: ".$replace_dirs."... Errors: ".$errors." ***</b>";
}

//*********************************************************//
//      this function rename all content in wordpress      //
//*********************************************************//
function q9_rename_all()
{
    if(!get_option('quiro9_wp_name_admin'))
    {
        global $replace_admin, $replace_config, $replace_includes, $replace_content, $replace_login;
        global $search_original_admin, $search_original_config, $search_original_includes, $search_original_content, $search_original_login;
        global $rename_admin, $rename_config, $rename_includes, $rename_content, $rename_login;

        $wp_back_dir = "..";
        // dirs need for renames a files (use the path retunrs in the previus fuctions)
        $rename_admin = [$wp_back_dir."\\".$search_original_admin,
            $wp_back_dir."\\".$replace_admin."\\css\\".$search_original_admin.".css",
            $wp_back_dir."\\".$replace_admin."\\css\\".$search_original_admin.".min.css",
            $wp_back_dir."\\".$replace_admin."\\css\\".$search_original_admin."-rtl.css",
            $wp_back_dir."\\".$replace_admin."\\css\\".$search_original_admin."-rtl.min.css",
            $wp_back_dir."\\".$search_original_includes."\\class-".$search_original_admin."-bar.php"];
        $rename_content = [$wp_back_dir."\\".$search_original_content,
            $wp_back_dir."\\".$replace_includes."\\js\\tinymce\\skins\\wordpress\\".$search_original_content.".css"];
        $rename_includes = [$wp_back_dir."\\".$search_original_includes];
        $rename_login = [$wp_back_dir."\\".$search_original_login];
        $rename_config = [$wp_back_dir."\\".$search_original_config];

        echo '<center>';
        $files = scanDir::scan($wp_back_dir, false, true);
        q9_search_replace($files,$search_original_admin,$replace_admin);
        q9_search_replace($files,$search_original_content,$replace_content);
        q9_search_replace($files,$search_original_login,$replace_login);
        q9_search_replace($files,$search_original_config,$replace_config);
        q9_search_replace($files,$search_original_includes,$replace_includes);

        // add values...
        add_option('quiro9_wp_name_admin', $replace_admin);
        add_option('quiro9_wp_name_content', $replace_content);
        add_option('quiro9_wp_name_includes', $replace_includes);
        add_option('quiro9_wp_name_login', $replace_login);
        add_option('quiro9_wp_name_config', $replace_config);

        // folders
        q9_rename_dirs($rename_admin,$search_original_admin,$replace_admin);
        q9_rename_dirs($rename_includes,$search_original_includes,$replace_includes);
        q9_rename_dirs($rename_login,$search_original_login,$replace_login);
        q9_rename_dirs($rename_config,$search_original_config,$replace_config);
        q9_rename_dirs($rename_content,$search_original_content,$replace_content);

        //redireccionamo al plugin de admin otra vez...
        echo '</center>';
        wp_die (__ ("<br/><a href='../".$replace_admin."/options-general.php?page=quiro9_rename_tool&status=q9_ok'><br/>Click here for it to work correctly...</a><br/>"));
    }
    else{wp_die (__ ('<br/>Error, No es posible aplicar los cambios... Ya se almacenaron datos<br/>'));}
}

//**************************************//
//      this function earse data        //
//**************************************//
function q9_data_earse(){
    if(get_option('quiro9_wp_name_admin'))
    {
        echo '<center>';
        //earse data for BD
        delete_option('quiro9_wp_name_admin');
        delete_option('quiro9_wp_name_content');
        delete_option('quiro9_wp_name_includes');
        delete_option('quiro9_wp_name_login');
        delete_option('quiro9_wp_name_config');
        echo '</center>';
        wp_die (__ ("<br/><a href='..'>Deleted data, please verify that everything works properly...</a><br/>"));
	}
else{wp_die (__ ('<br/>Error, No es posible aplicar los cambios... Parece no haber datos guardados...<br/>'));}
}

// modify only the dir take
function q9_new_update($get_dir)
{
        global $replace_admin, $replace_config, $replace_includes, $replace_content, $replace_login;
        global $search_original_admin, $search_original_config, $search_original_includes, $search_original_content, $search_original_login;
        echo '<center>';
        $files = scanDir::scan($get_dir, false, true);
        q9_search_replace($files,$search_original_content,$replace_content);
        q9_search_replace($files,$search_original_login,$replace_login);
        q9_search_replace($files,$search_original_config,$replace_config);
        q9_search_replace($files,$search_original_admin,$replace_admin);
        q9_search_replace($files,$search_original_includes,$replace_includes);
        echo '</center>';
}


?>
