<?php
/*  Derechos de Autor 2014 Juan Quiroga (correo electrónico: juanquiroga9@gmail.com)
   You should have received a copy of the GNU General Public License <https://www.gnu.org/copyleft/gpl.html>. */

// this class is a example for: gambit_642@hotmail.com . tanks!
// example from: http://php.net/manual/en/function.scandir.php
class scanDir {
    static private $wp_directories, $files, $ext_filter, $recursive;
    // scan(dirpath::string|array, extensions::string|array, recursive::true|false)
    static public function scan(){
        self::$recursive = false;
        self::$wp_directories = array();
        self::$files = array();
        self::$ext_filter = false;
        if(!$args = func_get_args())
        {
            die("Must provide a path string or array of path strings");
        }
        if(gettype($args[0]) != "string" && gettype($args[0]) != "array")
        {
            die("Must provide a path string or array of path strings");
        }
        if(isset($args[2]) && $args[2] == true){self::$recursive = true;}
        if(isset($args[1])){
            if(gettype($args[1]) == "array")
            {
				self::$ext_filter = array_map('strtolower', $args[1]);
			}
            else
            if(gettype($args[1]) == "string")
            {
				self::$ext_filter[] = strtolower($args[1]);
			}
        }
        self::verifyPaths($args[0]);
        return self::$files;
    }

    static private function verifyPaths($paths){
        $path_errors = array();
        if(gettype($paths) == "string"){$paths = array($paths);}

        foreach($paths as $path){
            if(is_dir($path))
            {
                self::$wp_directories[] = $path;
                $wp_dirContents = self::find_contents($path);
            }
            else
            {
                $path_errors[] = $path;
            }
        }
        if($path_errors)
        {
			echo "The following directories do not exists<br />";die(var_dump($path_errors));
		}
    }
    static private function find_contents($wp_dir){
        $result = array();
        $root = scandir($wp_dir);
        foreach($root as $value){
            if($value === '.' || $value === '..') {continue;}
            if(is_file($wp_dir.DIRECTORY_SEPARATOR.$value))
            {
                if(!self::$ext_filter || in_array(strtolower(pathinfo($wp_dir.DIRECTORY_SEPARATOR.$value, PATHINFO_EXTENSION)), self::$ext_filter))
                {
                    self::$files[] = $result[] = $wp_dir.DIRECTORY_SEPARATOR.$value;
                }
                continue;
            }
            if(self::$recursive)
            {
                foreach(self::find_contents($wp_dir.DIRECTORY_SEPARATOR.$value) as $value)
                {
                    self::$files[] = $result[] = $value;
                }
            }
        }
        return $result;
    }
}

//***************************************************************//
//    find and walk reclusive folders and files to replace...    //
//***************************************************************//
function q9_search_replace($files,$search,$replace)
{
    echo "<br/><br/><b> *** It was replaced in files: ".$search." for ".$replace." ***</b><br/>";
    $modify = array();$errors = 0;
    // walk on dir
    foreach($files as $libf)
    {       // this is a file
            $nochange = "/(\.po)|(\.mo)|(\.pot)|(\.png)|(\.svg)|(\.jpeg)|(\.jpg)|(\.bmp)|(\.png)|(\.gif)/i";
            if(preg_match($nochange,$libf)==0 and !strpos($libf,"quiro9-rename-tool"))
            {
                // open and read the file replace the content and write
                if ($gestor = fopen($libf,"r+b"))
                {
					$cont = fread($gestor,filesize($libf));
					fclose($gestor);

					$new_cont = str_replace($search,$replace,$cont,$this_replace);
					if($this_replace>0)
					{
						$gestor = fopen($libf,"w+b");
						fwrite($gestor, $new_cont);
						fclose($gestor);
					}
					array_push($modify,$libf);
                }
                else{echo "Error to read: ".$libf."<br/>";$errors++;}
             }
    }
    echo "<b> *** Finish: ".$replace."... Errors: ".$errors."  ***</b>";
    return $modify;
}

//*****************************************************************************//
//    this names search (original of wordpress or the old_modify to restore)   //
//*****************************************************************************//
function q9_global_orignal_vars(){
    global $search_original_admin, $search_original_config, $search_original_includes, $search_original_content, $search_original_login;

    // here is "original" path of URL for change... the coments is for the future!
    $search_original_admin = "wp-admin"; //admin: admin_url();
    $search_original_includes = "wp-includes"; //includes: includes_url();
    $search_original_content = "wp-content"; //content: content_url();
    $search_original_login = "wp-login.php"; //login
    $search_original_config = "wp-config.php"; //configure
}


//************************************************************************************************//
// CHECK THE NAMES (does not have special characters except '-' and that starts with a- z or 0-9) //
//************************************************************************************************//
function q9_validate($name_value)
{
    // Esto deberia evitar que el usuario ingrese cualquier simbolos (a exepcion de solo "-")
    $return = false;
    $initfor = "/^[a-z|0-9]/";
    if(preg_match($initfor,$name_value)>0 and !strpos($q9_up_dir,"$") and !strpos($q9_up_dir,"+") and !strpos($q9_up_dir,"&") and !strpos($q9_up_dir,"!") and !strpos($q9_up_dir,"%") and !strpos($q9_up_dir,"?") and !strpos($q9_up_dir,"#") and !strpos($q9_up_dir,"\\") and !strpos($q9_up_dir,"/") and !strpos($q9_up_dir,"*") and !strpos($q9_up_dir,"_"))
    {
		$return = true;
	}
    else
    {
		wp_die (__ ('<br/>ERROR, se detecto nombre no valido: \"'.$name_value.'\" - Operacion Detenida para evitar fallos<br/>'));
	}
    return $return;
}


?>
